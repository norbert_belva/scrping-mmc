const _ = require('lodash');
const fs = require('fs');
const request = require('superagent');
const trim = require('lodash.trim');
const startsWith = require('lodash.startswith');

const mcc_url = "https://www.mcc-mnc.com/"

const mccRelativeUrl = url =>
	_.isString(url) ? `${mcc_url}${url.replace(/^\/*?/, "/")}` : null;

request
.get(mccRelativeUrl)
.end(function (err, res) {
    if (err) {
        process.exit(1);
    }
    if (!res.ok) {
        process.exit(2);
    }
    fs.writeFileSync('response.html', res.text);
    parse(res.text);
});

function parse(content) {
    const regex = new RegExp(
       '<tr>'             +
       '<td>(\\d+)</td>'  +  
       '<td>([^<]+)</td>' +  
       '<td>([^<]+)</td>' +  
       '<td>([^<]+)</td>' + 
       '<td>([^<]*)</td>' + 
       '<td>([^<]*)</td>' + 
       '</tr>'
    );

    const lines = content.split('\n');

    const result = {};
    lines.forEach(function (line) {
        line = trim(line);

        if (!startsWith(line, '<tr>') || line.length < 5)
            return;

        const p = line.match(regex);
        if (!p) {
            console.errot("Parsing error")
            return;
        }

        const mcc = p[1];
        const mnc = (p[2] != 'n/a') ? p[2] : '';
        const mccmnc = mcc + mnc;
        const country_iso = (p[3] != 'n/a') ? p[3].toUpperCase() : null;
        const country_name = trim(p[4]);
        const country_code = trim(p[5]);
        const network_name = trim(p[6]);

        result[mccmnc] = {
            mcc,
            mnc,
            country_iso,
            country_name,
            country_code,
            network_name
        };
    });

    console.log(JSON.stringify(result, null, 4));
}




    
